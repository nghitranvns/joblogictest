//
//  AppCoordinator.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import Foundation

protocol AppCoordinator {
    func showCallList()
    func showCallDetail(customer: Customer)
    func showBuyList()
    func showBuyDetail(merchandise: Merchandise)
    func showSellList()
    func showSellDetail(merchandise: Merchandise)
}
