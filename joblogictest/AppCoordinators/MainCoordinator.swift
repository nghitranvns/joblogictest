//
//  MainCoordinator.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import Foundation
import UIKit

class MainCoordinator: AppCoordinator {
    
    public private(set) var mainNavigationController: UINavigationController
    public private(set) var mainViewController: MainViewController!
    
    init(withNavigationController nav: UINavigationController) {
        self.mainNavigationController = nav
        guard let rootViewController = nav.viewControllers.first as? MainViewController else {
            return
        }
        self.mainViewController = rootViewController
    }
    
    func showCallList() {
        let callViewModel = CallViewModel(withTitle: "Call List")
        let callVC = mainViewController.viewControllerFromStoryboard(vcKind: CallViewController.self)
        callVC.callViewModel = callViewModel
        callVC.dispatcher = self
        mainNavigationController.pushViewController(callVC, animated: true)
    }
    
    func showCallDetail(customer: Customer) {
        showMessage(withTitle: "Call Detail Demo", message: "You selected \(customer.name) with ID: \(customer.id)")
    }
    
    func showBuyList() {
        let buyViewModel = BuyViewModel(withTitle: "Buy List")
        let buyVC = mainViewController.viewControllerFromStoryboard(vcKind: BuyViewController.self)
        buyVC.buyViewModel = buyViewModel
        buyVC.dispatcher = self
        mainNavigationController.pushViewController(buyVC, animated: true)
    }
    
    func showBuyDetail(merchandise: Merchandise) {
        showMessage(withTitle: "Buy Detail Demo", message: "You selected \(merchandise.name) with ID: \(merchandise.id)")
    }
    
    func showSellList() {
        let sellViewModel = SellViewModel(withTitle: "Sell List")
        let sellVC = mainViewController.viewControllerFromStoryboard(vcKind: SellViewController.self)
        sellVC.sellViewModel = sellViewModel
        sellVC.dispatcher = self
        mainNavigationController.pushViewController(sellVC, animated: true)
    }
    
    func showSellDetail(merchandise: Merchandise) {
        showMessage(withTitle: "Sell Detail Demo", message: "You selected \(merchandise.name) with ID: \(merchandise.id)")
    }
    
    //MARK: - Utilities
    func showMessage(withTitle title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        mainViewController.present(alert, animated: true, completion: nil)
    }
}
