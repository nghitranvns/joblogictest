//
//  GlobalVariables.swift
//  LyraCareInstaller
//
//  Created by Tran Kien Nghi on 7/10/20.
//  Copyright © 2020 VEriK. All rights reserved.
//

import UIKit

struct GlobalVariables {
    static let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
}
