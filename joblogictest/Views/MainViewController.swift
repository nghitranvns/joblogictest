//
//  ViewController.swift
//  joblogictest
//
//  Created by nghitran on 14/02/2022.
//

import UIKit

class MainViewController: BaseViewController {
    
    @IBOutlet weak var mainView: MainView!
    let mainViewModel: MainViewModel! = MainViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let nav = self.navigationController else {
            return
        }
        self.dispatcher = MainCoordinator(withNavigationController: nav)
    }
    
    //MARK: - Custom UI configuration
    override func configureUI() {
        mainView.callButton.setTitle(mainViewModel.callButtonTitle, for: .normal)
        mainView.buyButton.setTitle(mainViewModel.buyButtonTitle, for: .normal)
        mainView.sellButton.setTitle(mainViewModel.sellButtonTitle, for: .normal)
        
        mainView.didCall = { [weak self] in
            self?.dispatcher?.showCallList()
        }
        mainView.didBuy = { [weak self] in
            self?.dispatcher?.showBuyList()
        }
        mainView.didSell = { [weak self] in
            self?.dispatcher?.showSellList()
        }
    }
}

