//
//  CustomerTableViewCell.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit

class CustomerTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    
    public private(set) var customer: Customer!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindCustomer(_ cus: Customer) {
        customer = cus
        nameLabel.text = "Name: " + customer.name
        numberLabel.text = "Number: " + customer.number
    }
    
    func cleanUp() {
        nameLabel.text = nil
        numberLabel.text = nil
    }
}
