//
//  BaseViewController.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit

class BaseViewController: UIViewController, StoryboardIdentifier {
    var dispatcher: AppCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        bindViewModel()
    }
    
    func configureUI() {}
    
    func bindViewModel() {}
}

// MARK: - Special Pop/Push
extension BaseViewController {
    func viewControllerFromStoryboard<T: UIViewController>(vcKind: T.Type) -> T where T: StoryboardIdentifier {
        guard let vc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: vcKind.storyboardId) as? T else {
            fatalError("View controller with storyboard id \(vcKind.storyboardId) isn't existed!!!")
        }
        return vc
    }

    func popToViewController<T: UIViewController>(vcKind: T.Type? = nil, animated: Bool = true) {
        guard let appDelegate = UIApplication.shared.delegate, let window = appDelegate.window else {
            return
        }
        if let vc = window?.rootViewController as? T {
            navigationController?.popToViewController(vc, animated: animated)
        } else if let vc = window?.rootViewController?.presentedViewController as? T {
            navigationController?.popToViewController(vc, animated: animated)
        } else if let vc = window?.rootViewController?.children.lazy.compactMap({ $0 as? T }).first {
            navigationController?.popToViewController(vc, animated: animated)
        }
    }
}
