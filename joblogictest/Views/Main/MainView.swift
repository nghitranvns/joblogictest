//
//  MainView.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit

class MainView: BaseView {

    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var sellButton: UIButton!

    // MARK: - Action callbacks
    var didCall: (() -> Void)? = nil
    var didBuy: (() -> Void)? = nil
    var didSell: (() -> Void)? = nil
    
    // MARK: - User events
    @IBAction func callButtonTapped(_ sender: Any) {
        didCall?()
    }
    
    @IBAction func buyButtonTapped(_ sender: Any) {
        didBuy?()
    }
    
    @IBAction func sellButtonTapped(_ sender: Any) {
        didSell?()
    }
}
