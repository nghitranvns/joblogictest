//
//  MerchandiseTableViewCell.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit

class MerchandiseTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    
    public private(set) var merchandise: Merchandise?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindMerchandise(_ mer: Merchandise) {
        merchandise = mer
        nameLabel.text = "Name: " + mer.name
        priceLabel.text = String(format: "Price: %.0f", mer.price)
        quantityLabel.text = "Quantily: \(mer.quantity)"
    }
    
    func cleanUp() {
        nameLabel.text = ""
        priceLabel.text = ""
        quantityLabel.text = ""
    }
}
