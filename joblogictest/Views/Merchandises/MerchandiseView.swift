//
//  MerchandiseView.swift
//  joblogictest
//
//  Created by nghitran on 16/02/2022.
//

import UIKit

class MerchandiseView: BaseView {
    @IBOutlet weak var tableView: UITableView!
    
    var merchandises: [Merchandise] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    var didSelectMerchandise: ((_ merchandise: Merchandise) -> Void)?
    
    override func setupView() {
        super.setupView()
        
        tableView.register(UINib(nibName: "MerchandiseTableViewCell", bundle: .main), forCellReuseIdentifier: "MerchandiseTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension MerchandiseView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return merchandises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MerchandiseTableViewCell", for: indexPath) as! MerchandiseTableViewCell
        
        cell.cleanUp()
        if indexPath.row < merchandises.count {
            let merchandise = merchandises[indexPath.row]
            cell.bindMerchandise(merchandise)
        }
        else {
            print("Fatal: Reuse CustomerTableViewCell without customer data")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < merchandises.count {
            let merchandise = merchandises[indexPath.row]
            didSelectMerchandise?(merchandise)
        }
    }
}
