//
//  CallViewController.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit
import ProgressHUD

class CallViewController: BaseViewController {
    
    @IBOutlet weak var callTableView: UITableView!
    
    var callViewModel: CallViewModel!

    override func viewDidLoad() {
        guard callViewModel != nil else {
            return
        }
        
        super.viewDidLoad()
    }
    
    override func configureUI() {
        self.navigationItem.title = callViewModel.title
        callTableView.register(UINib(nibName: "CustomerTableViewCell", bundle: .main), forCellReuseIdentifier: "CustomerTableViewCell")
        callTableView.delegate = self
        callTableView.dataSource = self
    }
    
    override func bindViewModel() {
        ProgressHUD.show("Loading call list...")
        callViewModel.customerReposistory.loadCustomers()
        
        // Listen for customer data changes
        callViewModel.customerReposistory.didLoadCustomers = { [weak self] (customers, error) in
            guard error == nil else {
                //TODO: Show error message if needed
                print("Cannot load call list with error: \(error!.localizedDescription)")
                return
            }
            
            DispatchQueue.main.async {
                ProgressHUD.dismiss()
                self?.callTableView.reloadData()
            }
        }
    }
}

extension CallViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return callViewModel.customerReposistory.numberOfCustomes()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerTableViewCell", for: indexPath) as! CustomerTableViewCell
        
        cell.cleanUp()
        if let customer = callViewModel.customerReposistory.customerAtIndex(indexPath.row) {
            cell.bindCustomer(customer)
        }
        else {
            print("Fatal: Reuse CustomerTableViewCell without customer data")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let customer = callViewModel.customerReposistory.customerAtIndex(indexPath.row) {
            dispatcher?.showCallDetail(customer: customer)
        }
    }
}
