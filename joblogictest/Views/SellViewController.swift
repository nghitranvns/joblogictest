//
//  SellViewController.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit
import ProgressHUD

class SellViewController: BaseViewController {
    
    @IBOutlet weak var merchandiseView: MerchandiseView!
    
    var sellViewModel: SellViewModel!
    
    override func viewDidLoad() {
        guard sellViewModel != nil else {
            return
        }
        
        super.viewDidLoad()
    }
    
    override func configureUI() {
        self.navigationItem.title = sellViewModel.title
        self.merchandiseView.didSelectMerchandise = { [weak self] merchandise in
            self?.dispatcher?.showSellDetail(merchandise: merchandise)
        }
    }
    
    override func bindViewModel() {
        // Listen for sell data changes
        sellViewModel.merchandiseReposistory.didLoadMerchandises = { [weak self] (merchandises, error) in
            guard error == nil else {
                //TODO: Show error message if needed
                print("Cannot load sell list with error: \(error!.localizedDescription)")
                return
            }
            
            DispatchQueue.main.async {
                //ProgressHUD.dismiss()
                self?.merchandiseView.merchandises = merchandises
            }
        }
        
        //ProgressHUD.show("Loading sell list...")
        sellViewModel.merchandiseReposistory.loadMerchandises()
    }
}
