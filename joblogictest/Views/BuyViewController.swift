//
//  BuyViewController.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit
import ProgressHUD

class BuyViewController: BaseViewController {

    @IBOutlet weak var merchandiseView: MerchandiseView!
    
    var buyViewModel: BuyViewModel!

    override func viewDidLoad() {
        guard buyViewModel != nil else {
            return
        }
        
        super.viewDidLoad()
    }
    
    override func configureUI() {
        self.navigationItem.title = buyViewModel.title
        self.merchandiseView.didSelectMerchandise = { [weak self] merchandise in
            self?.dispatcher?.showBuyDetail(merchandise: merchandise)
        }
    }
    
    override func bindViewModel() {
        ProgressHUD.show("Loading buy list...")
        buyViewModel.merchandiseReposistory.loadMerchandises()
        
        // Listen for buy data changes
        buyViewModel.merchandiseReposistory.didLoadMerchandises = { [weak self] (merchandises, error) in
            guard error == nil else {
                //TODO: Show error message if needed
                print("Cannot load buy list with error: \(error!.localizedDescription)")
                return
            }
            
            DispatchQueue.main.async {
                ProgressHUD.dismiss()
                self?.merchandiseView.merchandises = merchandises
            }
        }
    }
}
