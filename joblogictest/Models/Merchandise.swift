//
//  Merchandise.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import Foundation

protocol Merchandise {
    var id: Int { get }
    var name: String { get }
    var price: Double { get }
    var quantity: Int { get }
    var type: Int { get }
}

struct RawMerchandise: Merchandise {
    let id: Int
    var name: String
    var price: Double
    var quantity: Int
    let type: Int
}
