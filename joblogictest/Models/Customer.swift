//
//  Customer.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import Foundation

protocol Customer {
    var id: Int { get }
    var name: String { get }
    var number: String { get }
}

struct RawCustomer: Customer {
    let id: Int
    var name: String
    var number: String
}
