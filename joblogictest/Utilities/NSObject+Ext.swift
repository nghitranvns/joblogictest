//
//  NSObject+Extension.swift
//  Verik
//
//  Created by Nghi Tran Inc on 7/1/16.
//  Copyright © 2017 Verik. All rights reserved.
//

import Foundation

public extension NSObject {
    @objc class var nameOfClass: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    @objc var nameOfClass: String {
        return NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
    }
}
