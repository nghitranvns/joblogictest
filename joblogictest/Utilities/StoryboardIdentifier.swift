//
//  NSObject+Extension.swift
//  Verik
//
//  Created by Nghi Tran Inc on 7/1/16.
//  Copyright © 2017 Verik. All rights reserved.
//

protocol StoryboardIdentifier: AnyObject {
    static var storyboardId: String { get }
}

extension StoryboardIdentifier {
    static var storyboardId: String {
        return String(describing: self)
    }
}
