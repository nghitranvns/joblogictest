//
//  BaseView.swift
//  VEriK
//
//  Created by Nghi Tran Inc on 7/1/16.
//  Copyright © 2016 VEriK. All rights reserved.
//

import Foundation
import UIKit

class BaseView: UIView {
    
    @IBOutlet weak var contentView: UIView!;
    
    var baseNib: UINib? {
        let key = self.nameOfClass
        let nib = UINib.init(nibName: key, bundle: nil)
        return nib
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
        self.addConstraintForContent()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(frame: CGRect, useContentView: Bool) {
        self.init(frame: frame)
        if useContentView {
            self.setupView()
            self.addConstraintForContent()
        }
    }
    
    convenience init (withFrameForPopup frame: CGRect) {
        self.init(frame: frame)
    }
    
    deinit {
        print("========== " + self.nameOfClass + " deinit ==========")
    }
    
    func setupView() {
        let mainBundle = Bundle.main
        let key = self.nameOfClass
        guard mainBundle.path(forResource: key, ofType: "nib") == nil else  {
            guard self.contentView != nil else {
                if let nib = self.baseNib {
                    self.contentView = nib.instantiate(withOwner: self, options: nil).last as? UIView
                }
                
                return
            }
            
            return
        }
    }
    
    func addConstraintForContent() {
        guard self.contentView == nil else {
            self.addSubview(self.contentView!)
            self.contentView?.translatesAutoresizingMaskIntoConstraints = false
            
            self.addConstraint(self.constraint(toItem: self.contentView!, attribute: .top))
            self.addConstraint(self.constraint(toItem: self.contentView!, attribute: .left))
            self.addConstraint(self.constraint(toItem: self.contentView!, attribute: .bottom))
            self.addConstraint(self.constraint(toItem: self.contentView!, attribute: .right))
            
            return
        }
    }
    
    func constraint(toItem item: AnyObject, attribute: NSLayoutConstraint.Attribute) -> NSLayoutConstraint {
        return NSLayoutConstraint.init(item: self,
                                       attribute: attribute,
                                       relatedBy: .equal,
                                       toItem: item,
                                       attribute: attribute,
                                       multiplier: 1.0,
                                       constant: 0)
    }
}
