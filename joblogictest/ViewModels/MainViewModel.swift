//
//  MainViewModel.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit

class MainViewModel: NSObject {
    let callButtonTitle: String
    let buyButtonTitle: String
    let sellButtonTitle: String
    
    init(callTitle: String = "Call List", buyTitle: String = "Buy List", sellTitle: String = "Sell List") {
        callButtonTitle = callTitle
        buyButtonTitle = buyTitle
        sellButtonTitle = sellTitle
    }
}
