//
//  CallViewModel.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit

class CallViewModel: NSObject {
    let title: String
    var customerReposistory: CustomerRepository
    
    init(withTitle ttl: String, dataSource: CustomerRepository = CustomerRepositoryImpl()) {
        title = ttl
        customerReposistory = dataSource
    }
}
