//
//  SellViewModel.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit

class SellViewModel: NSObject {
    let title: String
    var merchandiseReposistory: MerchandiseRepository

    init(withTitle ttl: String, dataSource: MerchandiseRepository = MerchandiseRepositoryImpl_LocalStore()) {
        title = ttl
        merchandiseReposistory = dataSource
    }
}
