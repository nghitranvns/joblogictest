//
//  BuyViewController.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit

class BuyViewModel: NSObject {
    let title: String
    var merchandiseReposistory: MerchandiseRepository

    init(withTitle ttl: String, dataSource: MerchandiseRepository = MerchandiseRepositoryImpl()) {
        title = ttl
        merchandiseReposistory = dataSource
    }
}
