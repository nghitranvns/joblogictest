//
//  CustomerRepository.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit

protocol CustomerRepository {
    var didLoadCustomers: ((_ customers: [Customer], _ error: Error?) -> Void)? { get set }
    
    func customerList() -> [Customer]
    func numberOfCustomes() -> Int
    func loadCustomers()
    func customerAtIndex(_ index: Int) -> Customer?
}
