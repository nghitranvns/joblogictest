//
//  CustomerRepositoryImpl.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit
import Alamofire

class CustomerRepositoryImpl: CustomerRepository {
    var didLoadCustomers: (([Customer], Error?) -> Void)?
    
    private var customers: [Customer] = []
    private let webAPIService: WebAPIServiceImpl = WebAPIServiceImpl()
    
    func customerList() -> [Customer] {
        return customers
    }
    
    func numberOfCustomes() -> Int {
        return customers.count
    }
    
    func loadCustomers() {
        webAPIService.retrieveCustomers {[weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .failure:
                strongSelf.didLoadCustomers?([], NSError(domain: "Unknown", code: 0, userInfo: nil))
            case .notConnectedToInternet:
                strongSelf.didLoadCustomers?([], NSError(domain: "No internet", code: 0, userInfo: nil))
            case .success(responseData: let customers):
                strongSelf.customers = customers
                strongSelf.didLoadCustomers?(customers, nil)
            }
        }
    }
    
    func customerAtIndex(_ index: Int) -> Customer? {
        guard index >= 0 && index < customers.count else {
            return nil
        }
        
        return customers[index]
    }
}
