//
//  BuyMerchandise.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit
import ObjectMapper

class CloudMerchandise: NSObject, Mappable {
    var m_id: Int = 0
    var m_name: String = ""
    var m_price: Double = 0
    var m_quantity: Int = 0
    var m_type: Int = 0
   
    // MARK: Object Mapper
    required public init?(map: Map) {
        super.init()
        mapping(map: map)
    }
    
    // Mappable
    public func mapping(map: Map) {
        self.m_id <- map["id"]
        self.m_name <- map["name"]
        self.m_price <- map["price"]
        self.m_quantity <- map["quantity"]
        self.m_type <- map["type"]
    }
    
    override init() {
        m_id = 0
        m_name = ""
        m_price = 0
        m_quantity = 0
        m_type = 0
    }
}

extension CloudMerchandise: Merchandise {
    var id: Int { return m_id }
    var name: String { return m_name }
    var price: Double { return m_price }
    var quantity: Int { return m_quantity }
    var type: Int { return m_type }
}
