//
//  CloudCustomer.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import Foundation
import ObjectMapper

class CloudCustomer: NSObject, Mappable {
    var c_id: Int = 0
    var c_name: String = ""
    var c_number: String = ""
   
    // MARK: Object Mapper
    required public init?(map: Map) {
        super.init()
        mapping(map: map)
    }
    
    // Mappable
    public func mapping(map: Map) {
        self.c_id <- map["id"]
        self.c_name <- map["name"]
        self.c_number <- map["number"]
    }
    
    override init() {
        c_id = 0
        c_name = ""
        c_number = ""
    }
}

extension CloudCustomer: Customer {
    var id: Int { return c_id }
    var name: String { return c_name }
    var number: String { return c_number }
}
