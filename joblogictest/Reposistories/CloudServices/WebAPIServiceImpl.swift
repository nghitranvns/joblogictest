//
//  WebAPIServiceImpl.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit
import Alamofire
import ObjectMapper

class WebAPIServiceImpl: WebAPIService {
    static let endPoint = "https://my-json-server.typicode.com/imkhan334/demo-1/"
    
    func retrieveCustomers(completion: @escaping (WebAPIResponse<[Customer]>) -> Void) {
        AF.request(type(of: self).endPoint + "call").responseJSON
        { response in
            guard let urlResponse = response.response else {
                if .notConnectedToInternet == (response.error?.underlyingError as? URLError)?.code {
                    completion(.notConnectedToInternet)
                } else {
                    completion(.failure)
                }
                return
            }
            
            switch urlResponse.statusCode {
            case 200...299:
                if case .success(let json) = response.result,
                    let jsonDictionary = json as? [[String : Any]] {
                    let cuss = Mapper<CloudCustomer>().mapArray(JSONArray: jsonDictionary)
                    completion(.success(responseData: cuss as [Customer]))
                } else {
                    completion(.failure)
                }
            case NSURLErrorNotConnectedToInternet:
                completion(.notConnectedToInternet)
            default:
                completion(.failure)
            }
        }
    }
    
    func retrieveBuyMerchandise(completion: @escaping (WebAPIResponse<[Merchandise]>) -> Void) {
        AF.request(type(of: self).endPoint + "buy").responseJSON
        { response in
            guard let urlResponse = response.response else {
                if .notConnectedToInternet == (response.error?.underlyingError as? URLError)?.code {
                    completion(.notConnectedToInternet)
                } else {
                    completion(.failure)
                }
                return
            }
            
            switch urlResponse.statusCode {
            case 200...299:
                if case .success(let json) = response.result,
                    let jsonDictionary = json as? [[String : Any]] {
                    let mchds = Mapper<CloudMerchandise>().mapArray(JSONArray: jsonDictionary)
                    completion(.success(responseData: mchds as [CloudMerchandise]))
                } else {
                    completion(.failure)
                }
            case NSURLErrorNotConnectedToInternet:
                completion(.notConnectedToInternet)
            default:
                completion(.failure)
            }
        }
    }
}
