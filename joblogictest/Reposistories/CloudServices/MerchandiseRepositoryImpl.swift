//
//  MerchandiseRepositoryImpl.swift
//  joblogictest
//
//  Created by nghitran on 16/02/2022.
//

import UIKit

class MerchandiseRepositoryImpl: MerchandiseRepository {
    var didLoadMerchandises: (([Merchandise], Error?) -> Void)?
    
    private var merchandises: [Merchandise] = []
    private let webAPIService: WebAPIServiceImpl = WebAPIServiceImpl()
    
    func merchandiseList() -> [Merchandise] {
        return merchandises
    }
    
    func numberOfMerchandises() -> Int {
        return merchandises.count
    }
    
    func loadMerchandises() {
        webAPIService.retrieveBuyMerchandise {[weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .failure:
                strongSelf.didLoadMerchandises?([], NSError(domain: "Unknown", code: 0, userInfo: nil))
            case .notConnectedToInternet:
                strongSelf.didLoadMerchandises?([], NSError(domain: "No internet", code: 0, userInfo: nil))
            case .success(responseData: let merchandises):
                strongSelf.merchandises = merchandises
                strongSelf.didLoadMerchandises?(merchandises, nil)
            }
        }
    }
    
    func merchandiseAtIndex(_ index: Int) -> Merchandise? {
        guard index >= 0 && index < merchandises.count else {
            return nil
        }
        
        return merchandises[index]
    }
}
