//
//  MerchandiseRepository.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit

protocol MerchandiseRepository {
    var didLoadMerchandises: ((_ merchandises: [Merchandise], _ error: Error?) -> Void)? { get set }
    
    func merchandiseList() -> [Merchandise]
    func numberOfMerchandises() -> Int
    func loadMerchandises()
    func merchandiseAtIndex(_ index: Int) -> Merchandise?
}
