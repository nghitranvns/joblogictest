//
//  LocalStoreMerchandise.swift
//  joblogictest
//
//  Created by nghitran on 16/02/2022.
//

import UIKit
import CoreData

public final class LocalStoreMerchandise: NSManagedObject {
    @NSManaged public var m_id: NSNumber?
    @NSManaged public var m_name: String?
    @NSManaged public var m_price: NSNumber?
    @NSManaged public var m_quantity: NSNumber?
    @NSManaged public var m_type: NSNumber?
}

extension LocalStoreMerchandise: Merchandise {
    var id: Int { return m_id?.intValue ?? 0 }
    var name: String { return m_name ?? "" }
    var price: Double { return m_price?.doubleValue ?? 0 }
    var quantity: Int { return m_quantity?.intValue ?? 0 }
    var type: Int { return m_type?.intValue ?? 0 }
}
