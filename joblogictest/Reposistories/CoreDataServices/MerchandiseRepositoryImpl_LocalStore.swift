//
//  MerchandiseRepositoryImpl_LocalStore.swift
//  joblogictest
//
//  Created by nghitran on 16/02/2022.
//

import UIKit
import MagicalRecord

class MerchandiseRepositoryImpl_LocalStore: MerchandiseRepository {
    var didLoadMerchandises: (([Merchandise], Error?) -> Void)?
    
    private var merchandises: [Merchandise] = []
    
    func merchandiseList() -> [Merchandise] {
        return merchandises
    }
    
    func numberOfMerchandises() -> Int {
        return merchandises.count
    }
    
    func loadMerchandises() {
        let request = LocalStoreMerchandise.mr_requestAllSorted(by: "m_id", ascending: true)
        
        request.returnsDistinctResults = true
        request.resultType = .managedObjectResultType
        
        do {
            let result = try NSManagedObjectContext.mr_default().fetch(request)
            let merchandises = (result as! [NSManagedObject]).map({ $0 as! LocalStoreMerchandise })
            self.merchandises = merchandises
            self.didLoadMerchandises?(merchandises, nil)
        } catch {
            self.didLoadMerchandises?([], error)
        }
    }
    
    func merchandiseAtIndex(_ index: Int) -> Merchandise? {
        guard index >= 0 && index < merchandises.count else {
            return nil
        }
        
        return merchandises[index]
    }
    
    //MARK: - Insert mock data for testing
    init() {
        configureCoreDataStack()
        
        //Generate data for testing
        let isMackDataGeneratedKey = "isMackDataGenerated"
        let userDefaultd = UserDefaults.standard
        if userDefaultd.bool(forKey: isMackDataGeneratedKey) == false {
            initializeLocalData()
            userDefaultd.set(true, forKey: isMackDataGeneratedKey)
            userDefaultd.synchronize()
        }
    }

    func initializeLocalData() {
        let rauMerchandises: [RawMerchandise] = [
            RawMerchandise(id: 3, name: "Table", price: 12000, quantity: 1, type: 2),
            RawMerchandise(id: 2, name: "TV", price: 38000, quantity: 2, type: 2),
            RawMerchandise(id: 1, name: "iPhone X", price: 150000, quantity: 1, type: 2)
        ]
        
        save(merchandises: rauMerchandises) { success in
            if success {
                print("Successfully create mock date in CoreData")
            }
            else {
                print("Failed to create mock date in CoreData")
            }
        }
    }
    
    func save(merchandises: [Merchandise], completion: @escaping (Bool) -> Void) {
        MagicalRecord.save({ (ctx) in
            for mch in merchandises {
                let entity = LocalStoreMerchandise.mr_createEntity(in: ctx)
                
                entity?.m_id = NSNumber(value: mch.id)
                entity?.m_name = mch.name
                entity?.m_price = NSNumber(value: mch.price)
                entity?.m_quantity = NSNumber(value: mch.quantity)
                entity?.m_type = NSNumber(value: mch.type)
            }
        }) { (succeed, _) in
            completion(succeed)
        }
    }
}

private extension MerchandiseRepositoryImpl_LocalStore {
    func configureCoreDataStack() {
        MagicalRecord.setupCoreDataStack()
    }
}
