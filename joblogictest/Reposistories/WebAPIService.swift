//
//  WebAPIService.swift
//  joblogictest
//
//  Created by nghitran on 15/02/2022.
//

import UIKit

enum WebAPIResponse<T> {
    case failure
    case notConnectedToInternet
    case success(responseData: T)
}

protocol WebAPIService {
    func retrieveCustomers(completion: @escaping (WebAPIResponse<[Customer]>) -> Void)
    func retrieveBuyMerchandise(completion: @escaping (WebAPIResponse<[Merchandise]>) -> Void)
}
